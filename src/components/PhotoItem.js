import React from 'react';

class PhotoItem extends React.Component {
    // React 클래스 안에 Component를 상속 받겠다

    render() {
        const { itemName, itemCount } = this.props;
        // { 객체 두 개 받을거야 }
        // { 스코프 }말고 [ 배열 ]이 올 수도 있음
        // this.props 부터 실행
        // const는 스코프 안 객체에 걸린게 아니라 { }에 걸린거다 => "두 개만 받을거야"

        return(
            <div>
                <div className="puppy-name">🐾 이름 : {itemName}</div>
                <div className="puppy-age">🐾 나이 : {itemCount} 세</div>
            </div>
        );
    }
}

export default PhotoItem;
// 나가서 일하렴